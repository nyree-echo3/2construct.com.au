@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
        <div class="col-sm-3 offset-sm-1 blog-sidebar">         
		  <div class="sidebar-module">
			<h4>Invitation</h4>       
		  </div>
		</div>
       
        <div class="col-sm-8 blog-main">

          <div class="blog-post">            
            <h1 class="blog-post-title">Invitation</h1>
            
			<h2>Thanks for RSVPing!</h2>
            
            @if ($response == "yes")  
			    <p>We're so glad that you can make it.  Look forward to seeing you on the night. </p>
			@else 
			   <p>We're sorry to hear that you can't make it.</p>
			@endif
			
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
