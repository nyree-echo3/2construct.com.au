<?php 
   // Set Meta Tags
   $meta_title_inner = $project_item->meta_title; 
   $meta_keywords_inner = $project_item->meta_keywords; 
   $meta_description_inner = $project_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

@section('styles')
    <link rel="stylesheet" href="{{ asset('/components/baguettebox.js/src/baguetteBox.css') }}">
@endsection


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-projects')        
        
        <div class="col-sm-9 blog-main">
           <section class="project-block cards-project">                           
                 <div class="blog-post">                                                          
						<div class='project-item'>
						   <div class='project-item-txt'>
						      <h1 class="blog-post-title">{{ $project_item->title }}</h1>
						      
						      @if ( $project_item->entrant != "")<div class='project-value'><strong>Entrant</strong><br>{{$project_item->entrant}}</div>@endif 
						      @if ( $project_item->award != "")<div class='project-value'><strong>Award</strong><br>{{$project_item->award}}</div>@endif 
						      @if ( $project_item->award_category != "")<div class='project-value'><strong>Category</strong><br>{!! $project_item->award_category !!}</div>@endif 
						      
						      @if ( $project_item->client != "")<div class='project-value'><strong>Client</strong><br>{{$project_item->client}}</div>@endif 
							  @if ( $project_item->location != "")<div class='project-value'><strong>Location</strong><br>{{$project_item->location}}</div>@endif 
					          @if ( $project_item->completion_date != "")<div class='project-value'><strong>Completion</strong><br>{{date("F Y", strtotime($project_item->completion_date)) }}</div>@endif 
							  
							   @if ( $project_item->client != "" || $project_item->location != "" || $project_item->completion != "")<div class='project-value-spacer'></div>@endif 
							   
							  {!! $project_item->description !!}
							  
							  <div class='btn-back'>
							     <a class='btn-back' href='javascript: window.history.go(-1);'>< Back</a>	
							  </div>
						   </div>

						   @if (count($project_item->images) > 0)
							   <div class='project-item-img'>							   							   							   
							   
									<!-- Carousel of images -->
									<div id="carousel-projects" class="carousel slide carousel-fade" data-ride="carousel">

									  <!-- Indicators -->
									  <ul class="carousel-indicators">
										@php $counter = 0;
										@endphp

										@foreach($project_item->images as $image)   
										   <li data-target="#carousel-projects" data-slide-to="{{ $counter }}" class="{{ $counter == 0 ? 'active' : '' }}"></li>   

										   @php 
										   $counter++;         	
										   @endphp
										@endforeach

									  </ul>

									  <!-- The slideshow -->
									  <div class="carousel-inner">
										@php $counter = 0;
										@endphp

										@foreach($project_item->images as $image)  
											@php 
											$counter++;         	
											@endphp

											<div class="carousel-item {{ $counter == 1 ? ' active' : '' }}">
											  <!--<a class="lightbox" href="{{ url('') }}{{$image->location}}" data-caption="{{$project_item->title}}">-->
												 <img src="{{ url('') }}{{$image->location}}" alt="{{$project_item->name}}">
											  <!--</a>-->
											</div>	
										@endforeach	
									  </div>

									  <!-- Left and right controls -->
									  <a class="carousel-control-prev" href="#carousel-projects" data-slide="prev">
										<span class="carousel-control-prev-icon"></span>
									  </a>
									  <a class="carousel-control-next" href="#carousel-projects" data-slide="next">
										<span class="carousel-control-next-icon"></span>
									  </a>

									</div>
									<!-- End of Carousel of images -->

						   @endif

						</div>         	               	        			                          
           </section>      	   
          	                       					  								  			            
       </div><!-- /.blog-post -->         
    </div><!-- /.blog-main -->        

  </div><!-- /.row -->

</div><!-- /.container -->




@section('scripts')
    <script src="{{ asset('/components/baguettebox.js/src/baguetteBox.js') }}"></script>
@endsection


@section('inline-scripts')
   <script type="text/javascript">
        $(document).ready(function () {       
           baguetteBox.run('.cards-project', { animation: 'slideIn'});
        });	   	   
    </script>			
@endsection