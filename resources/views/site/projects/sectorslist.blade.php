        
<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest Projects" ? $category_name : $category_name . " - Projects"); 
   $meta_keywords_inner = "Projects"; 
   $meta_description_inner = ($category_name == "Latest Projects" ? $category_name : $category_name . " - Projects");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        @include('site/partials/sidebar-projects')
        
        <div class="col-sm-9 blog-main">
                   
          <div class="blog-post">           
            <h1 class="blog-post-title">{{ $category_name }}</h1>
            <h2>{{ $sector_name }}</h2>
            <section class="project-block cards-project">
               <div class="container">	  
                  <div class="row">	       	            
	              
            @if(isset($items))            
                  @foreach($items as $item)        
                      <div class="col-sm-4">
						   <a href="{{ url('') }}/projects/{{ $item->category->slug }}/{{$item->slug}}">
						   <div class="panel-news-item">	
								<div class="div-img">
								@if (count($item->images) > 0)	
								   <img src="{{ url('') }}{{$item->images[0]->location}}" alt="{{$item->images[0]->name}}" />	
								@endif
								</div>				                                    

								<!--<div class="panel-news-item-category-date">
									<span class="panel-news-item-category">{{ $item->category->name }}</span>
									<span class="panel-news-item-date">{{date("M Y", strtotime($item->start_date))}}</span>
								</div>-->
								<div class="panel-news-item-title">{{$item->title}}</div>
								<div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>

								<div><a class="projects-more" href='{{ url('') }}/projects/{{ $item->category->slug }}/{{$item->slug}}'>Read More ></a></div>		
						  </div>                                                 
						  </a>
						</div>                 														                                                    
                  @endforeach

              
               @else
                 <p>Currently there is no projects to display.</p>    
               @endif
              </div><!-- /.row -->
                      </div><!-- /.container -->			             
                   </section>  
                                         
         </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
