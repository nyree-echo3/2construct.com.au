@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-pages')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post row">
                        <div class="col-12">
                           <!-- <h1 class="blog-post-title">Executive Team</h1>-->
						</div>
                       
                        @if($items)
                            @php $currentCategory = "" @endphp
  
                            @foreach($items as $item)
                                @if ($currentCategory == "" || $currentCategory != $item->category->name)
                                   @if ($currentCategory != "")  
                                      <div class="col-11 team-hr">
                                         <hr>
						              </div>
                                   @endif
                                   
                                   <div class="col-12">                                      
                                      <h1 class="blog-post-title team-h2 {{ ($currentCategory != "" ? 'team-h2-gap' : '') }}">{{ $item->category->name }}</h1>
						           </div>
                                   
                                   @php $currentCategory = $item->category->name @endphp
                                @endif
                                
                                
								<div class="col-lg-4 team-item">
								  <a href='{{ ($item->category->slug == "old-2construct-board" ? 'javascript:void(0)' : url('') . '/team/' . $item->category->slug . '/' . $item->slug) }}'>				
									   <div class="team-a">
										 <div class="div-img">
											<img src="{{ url('') }}/{{ $item->photo }}" alt="{{ $item->name }}"> 
										 </div>
										 <div class="team-txt">
											<div class="team-name-band-name">{{ $item->name }}</div>
											<div class="team-name-band-title">{!! $item->job_title !!}</div>
										 </div>   
									   </div>							
								   </a>     			
								</div>                                                                     
                                
                            @endforeach

                        @else
                            <p>Currently there is no team member to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
