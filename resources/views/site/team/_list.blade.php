@extends('site/layouts/app')

@section('content')

    @include('site/partials/carousel-inner')

    <div class="blog-masthead ">
        <div class="container">

            <div class="row">
                @include('site/partials/sidebar-pages')

                <div class="col-sm-9 blog-main">

                    <div class="blog-post row">
                        <div class="col-12">
                           <h1 class="blog-post-title">Executive Team</h1>
						</div>
                       
                        @if($items)
                            @php $currentCategory = "" @endphp
  
                            @foreach($items as $item)
                                @if ($currentCategory == "" || $currentCategory != $item->category->name)
                                   <div class="col-12">
                                      <h2 class="blog-post-title team-h2">{{ $item->category->name }}</h2>
						           </div>
                                   
                                   @php $currentCategory = $item->category->name @endphp
                                @endif
                                
                                <a href="{{ url('') }}/team/{{ $item->category->slug }}/{{$item->slug}}">
                                    <div class="col-xs-1 col-sm-6 col-lg-4 team-item">
                                        <div class="team-item-img" style="background: url({{ $item->photo }}); "></div>
                                        <div class="team-name-band">
                                            <a href="{{ url('') }}/team/{{ $item->category->slug }}/{{$item->slug}}">
                                            <p>{{ $item->name }}</p>
                                            <p><span class="team-name-band-title">{{ $item->job_title }}</span></p>
                                            </a>
                                        </div>
                                    </div>
                                </a>
                            @endforeach

                        @else
                            <p>Currently there is no team member to display.</p>
                        @endif

                    </div><!-- /.blog-post -->
                </div><!-- /.blog-main -->

            </div><!-- /.row -->

        </div><!-- /.container -->
    </div>
@endsection
