@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">
         @include('site/partials/sidebar-pages')        
        
        <div class="col-sm-9 blog-main">

          <div class="blog-post testimonials">  
            <h1 class="blog-post-title">Testimonials</h1>        
            
            @if (isset($items))        
				@foreach ($items as $item)	
			        <div class='testimonials-item'>          
			           <div class='testimonials-item-txt'>                       
				          {!! $item->description !!}	
					   </div>		
					   <div class='testimonials-item-person'>  	    				
					      {{$item->person}}					
					   </div>					
			        </div>
				@endforeach 
            @endif			     
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection
