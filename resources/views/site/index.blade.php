@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel')

<div id="divIndex">
	@include('site/partials/index-company')
	@include('site/partials/index-pages')
	@include('site/partials/index-video')

	@include('site/partials/index-projects')
	@include('site/partials/index-awards')
</div>

@endsection
