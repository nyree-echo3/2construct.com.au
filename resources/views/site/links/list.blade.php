        
<?php 
   // Set Meta Tags
   $meta_title_inner = "Links"; 
   $meta_keywords_inner = "Links"; 
   $meta_description_inner = "Links"; 
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        <div class="col-lg-12 blog-main">
                   
          <div class="blog-post links-div">                            
               <div class="container">	  
                  <div class="row">	       	            
	                    <div class="col-lg-6 link-col">
							@if (isset($client_links))
						       <div class="container">	  
                                 <div class="row links-col-border">	
                                      <div class="col-lg-12 links-hdr">
                                        <h1>{{ $client_links[0]->category->name }}</h1>
									  </div> 
									   
									  @php
									  $item_counter = 0;									  
									  $col_position = 0;
									  $items_total = sizeof($client_links);
									  $items_last_row = $items_total % 4;
									  
									  @endphp
									                                                                                                                                                                 
									  @foreach($client_links as $item)  
									      @php
									      $item_counter++;
									      $col_position++;
									      
									      if ($col_position == 5) {
									         $col_position = 1;
									      }
									      @endphp
									      									    
										  <div class="links-img {{ ($item_counter <= 4 ? 'links-img-top-border' : '') }} {{ ($col_position == 1 ? 'links-img-left-border' : '') }} {{ ($col_position == 4 ? 'links-img-right-border' : '') }} {{ ($item_counter > $items_total - $items_last_row ? 'links-img-bottom-border' : '') }} {{$items_total}}">										  
											 <a href="{{ $item->url }}" target="_blank"><img src="{{ url('') }}{{$item->image }}" alt="{{$item->name}}"></a>											
										  </div>
										  
										  @if ($col_position < 4)
										     <div class="links-img-divider-side"></div> 
										  @endif
									                  														                                                    
									       @if ($item_counter % 4 == 0)
								             <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										  @endif            														                                                    
										                  														                                                    
									  @endforeach 
								 </div>
							   </div>                                  	            	                     
							@endif
					   </div>
               
                       <div class="col-lg-6 link-col">
							@if (isset($partner_links))
						       <div class="container">	  
                                 <div class="row">	
                                      <div class="col-lg-12 links-hdr links-hdr2">
                                        <h1>{{ $partner_links[0]->category->name }}</h1>
									  </div> 
									   
									  @php
									  $item_counter = 0;									  
									  $col_position = 0;
									  $items_total = sizeof($client_links);
									  $items_last_row = $items_total % 4;
									  
									  @endphp
									                                                                                                                                                                 
									  @foreach($partner_links as $item)  
									      @php
									      $item_counter++;
									      $col_position++;
									      
									      if ($col_position == 5) {
									         $col_position = 1;
									      }
									      @endphp
									      									    
										  <div class="links-img {{ ($item_counter <= 4 ? 'links-img-top-border' : '') }} {{ ($col_position == 1 ? 'links-img-left-border' : '') }} {{ ($col_position == 4 ? 'links-img-right-border' : '') }} {{ ($item_counter > $items_total - $items_last_row ? 'links-img-bottom-border' : '') }} {{$items_total}}">										  
											 <a href="{{ $item->url }}" target="_blank"><img src="{{ url('') }}{{$item->image }}" alt="{{$item->name}}"></a>											
										  </div>
										  
										  @if ($col_position < 4)
										     <div class="links-img-divider-side"></div> 
										  @endif
									                  														                                                    
									      @if ($item_counter % 4 == 0)
								             <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										     <div class="links-col-3"><div class="links-img-divider-bottom"></div></div>
										  @endif            														                                                    
										                  														                                                    
									  @endforeach 
								 </div>
							   </div>                                  	            	                     
							@endif
					   </div>
                
                 </div><!-- /.row -->
              </div><!-- /.container -->			             
                     
                                         
         </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->

@endsection
