<!-- Navbar -->

<nav class="navbar navbar-expand-lg fixed-top btco-hover-menu">	
<div class="panelNav">    
     <div class="navbar-logo">
     <a href="{{ url('') }}" title="{{ $company_name }}"><img src="{{ url('') }}/images/site/logo.png" title="{{ $company_name }}" alt="{{ $company_name }}"></a>
  </div>
  
	<button class="navbar-toggler custom-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse" aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>
	<div class="collapse navbar-collapse" id="navbarCollapse">
		<ul class="navbar-nav mr-auto">
			<!-- <li class="nav-item {{ (!isset($page_type) ? "active" : "") }}">
				<a class="nav-link" href="{{ url('') }}"><i class='fa fa-home'></i> <span class="sr-only">(current)</span></a>
			</li> -->
			@if(count($navigation))
               @foreach($navigation as $nav_item)    
                    <!--
                    @if ($nav_item["slug"] == "projects")
                        <li class="nav-item dropdown">
						   <a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('') }}/sectors" id="navbarDropdownMenuLink">Sectors</a>
                       
                           <ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/aged-care">Aged Care</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/civic">Civic</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/civil-and-infrastructure">Civil & Infrastructure</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/commercial">Commerical</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/community">Community</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/education">Education</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/Industrial">Industrial</a></li>
                               <li><a class="dropdown-item" href="{{ url('') }}/sectors/sport-and-recreation">Sport & Recreation</a></li>
                           </ul> 
                        </li>
                    @endif
                    -->
                    
					<li class="nav-item dropdown">
						<a class="nav-link {{ (isset($page_type) && (($page_type == "Pages" && $category[0]->slug == $nav_item["slug"]) || ($page_type != "Pages" && $page_type == $nav_item["name"])) ? "active" : "") }}" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}" id="navbarDropdownMenuLink">
							{{ $nav_item["display_name"] }}
						</a>
						
						@if(isset($nav_item["nav_sub"]) && sizeof($nav_item["nav_sub"]) > 0)
							<ul class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
							    
						    
							    @foreach($nav_item["nav_sub"] as $nav_sub)						                               
							        @if ($nav_item["slug"] == "about" & $nav_sub["slug"] == "community")									
								         <li><a class="dropdown-item" href="{{ url('') }}/projects/awards">Awards</a></li>
									     <!--<li><a class="dropdown-item" href="{{ url('') }}/testimonials">Testimonials</a></li>-->																		
								    @endif
							    	
								    @if (($nav_item["slug"] != "projects") || ($nav_item["slug"] == "projects" && $nav_sub["slug"] != "awards"))

										@if($nav_sub["slug"]=='our-team')
											<li><a class="dropdown-item" href="{{ url('') }}/team/2construct-board">2Construct Board</a>
											<li><a class="dropdown-item" href="{{ url('') }}/team/2construct-executive-team">2Construct Executive Team</a>
										@else
										<li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub["slug"] }}">{{ (isset($nav_sub["title"]) ? $nav_sub["title"] : $nav_sub["name"]) }}</a>
										@endif

											@if(isset($nav_item["nav_sub_sub"]))
												<ul class="dropdown-menu">
													@foreach($nav_item["nav_sub_sub"] as $nav_sub_sub)	
													   @if($nav_sub_sub["parent_page_id"] == $nav_sub["id"])														  
														  <li><a class="dropdown-item" href="{{ url('') }}/{{ ($nav_item["name"] == "Pages" ? "pages/" : "") }}{{ $nav_item["slug"] }}/{{ $nav_sub_sub["slug"] }}">{{ (isset($nav_sub_sub["title"]) ? $nav_sub_sub["title"] : $nav_sub_sub["name"]) }}</a></li>
													   @endif
													@endforeach											
												</ul>
											@endif
											
											@if ($nav_item["slug"] == "projects" && $nav_sub["slug"]  == "building-projects")
								                
									            @switch ($nav_sub["id"])
									               @case (1) @php $project_sectors = $project_sectors_civil @endphp @break
									               @case (2) @php $project_sectors = $project_sectors_building @endphp @break
									               @case (3) @php $project_sectors = $project_sectors_civic @endphp @break									               
									            @endswitch										       										       
										       
										        @if (isset($project_sectors))
													<ul class="dropdown-menu">		
													   @foreach ($project_sectors as $project_sector)										
													      <li><a class="dropdown-item" href="{{ url('') }}/sectors/{{ $nav_sub["slug"]}}/{{ str_replace(" ", "-", strtolower($project_sector["sector"])) }}">{{ $project_sector["sector"] }}</a></li>
													   @endforeach												   												 
													</ul>						
											    @endif						
											@endif

										</li>
									@endif
								@endforeach																
								
							</ul>
						@endif
						
					</li>										
			   @endforeach

			   <li class="nav-item dropdown">
				   <a class="nav-link " href="{{ url('contact') }}" id="navbarDropdownMenuLink">Contact</a>
			   </li>

           <!--<li><a href='tel:{{ str_replace(' ', '', $phone_number) }}' class="nav-link navbar-contacts">{{ $phone_number }}</a>	</li>-->
            @endif
                                    			
		</ul>	  		  
			<!--<div class='navbar-contacts'>
			  <a href='tel:{{ str_replace(' ', '', $phone_number) }}' class="nav-link"><i class='fa fa-mobile-alt'></i> {{ $phone_number }}</a>			  
			</div>-->
	</div>
	</div>
</nav>
