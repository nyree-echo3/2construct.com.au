<div id="myCarousel" class="carousel slide" data-ride="carousel">
  <div class="carousel-inner inside-page">           
			<div class="carousel-item active">
		  	
		      @php
	            $imgUrl = "media/Sliders/header.jpg";
	            $imgAlt = "";
	            
	            // Page Module Category
		        if (isset($category) && sizeof($category) > 0)  {
	               if ($page->header == "")  {
					   $imgUrl = $category[0]->header;
					   $imgAlt = $category[0]->name;
		           } else {
		               $imgUrl = $page->header;
					   $imgAlt = $page->title;
		           }
		        }	
		       
		        // Module
		        if (isset($module) && $module->name != "Project")  {	         
		           $imgUrl = $module->header_image;
		           $imgAlt = $module->display_name;
		        } elseif  (isset($module) && $module->name == "Project") {
	               foreach ($side_nav as $item)  {
		              if ($item->slug == $category_slug)  {
		              $imgUrl = $item->header;
		              $imgAlt = $item->name;
		              }
		           }
		        }
		        	 	        	        
		      @endphp
		      
			  <img class="slide" src="{{ url('') }}/{{ $imgUrl }}" alt="{{ $imgAlt }}">
			  <div class="container">
				<div class="carousel-caption text-left">					  
				</div>
			  </div>
			</div>
       
  </div>
</div>