<footer class='footer'>
 <div class='footerContactsWrapper'>
	 <div class='footerContacts'>
		<div class="row">
       
	       <div class="col-lg-3 col-full">
			  <div class="footer-newsletter">
				  <h2>Subscribe to our newsletter</h2>
					  <div class="footer-newsletter-controls">
					  
					     <form id="subForm" class="js-cm-form" action="https://www.createsend.com/t/subscribeerror?description=" method="post" data-id="5B5E7037DA78A748374AD499497E309ED723A458F0889827FFFE3367E56CE4B2862728D9302A741E78C264CB798C8115F00E170A40E1C3E9230197F8B870C962">	
							    <div class="footer-newsletter-input">
								   <label for="fieldName">Name</label>								
								   <input id="fieldName" name="cm-name" type="textbox" />
							    </div>
							
							    <div class="footer-newsletter-input">
								   <label for="fieldEmail">Email</label>								
								   <input id="fieldEmail" name="cm-itkukkl-itkukkl" type="email" class="js-cm-email-input" required />
							    </div>
							
							    <div class="footer-newsletter-btn">
								   <button class="js-cm-submit-button btn-submit" type="submit">GO</button>
							    </div>
						</form>
						
                        <script type="text/javascript" src="https://js.createsend1.com/javascript/copypastesubscribeformlogic.js"></script>
					  					  					  					  
				  </div>
				  
			  </div>
		   </div>  		   		   

		   <div class="col-lg-2 col-border col-full">
		   <div class="center-block">
			  {!! $contact_details !!}
			   </div>
		   </div>

			<div class="col-lg-2 col-border col-full">
				<div class="center-block">
					<h2>Geelong Office</h2>
					<p>Suite 2. Level 1, 37 Malop Street,<br />Geelong VIC 3220</p>
				</div>
			</div>

			<div class="col-lg-2 col-border col-full">
			  @if ( $phone_number != "") Telephone: <a href="tel:{{ str_replace(" ", "", $phone_number) }}">{{ $phone_number }}</a><br> @endif 
			  @if ( $contact_email != "") Email: <a href="mailto:{{ $contact_email }}">{{ $contact_email }}</a> @endif 			  
		   </div> 
		   
		   <div class="col-lg-2 col-border col-full">ap
			  <img  src="{{ url('') }}/images/site/certification.png" text="Certifications" alt="Certifications" />
		   </div>  
		   
		   <div class="col-lg-1 col-border col-full">			  
			  <div id="footer-social">	
			    <h2>Follow Us</h2>						
				@if ( $social_instagram != "") <a href="{{ $social_instagram }}" target="_blank"><i class='fab fa-instagram'></i></a> @endif 
				@if ( $social_linkedin != "") <a href="{{ $social_linkedin }}" target="_blank"><i class='fab fa-linkedin-in'></i></a> @endif            				
				@if ( $social_youtube != "") <a href="{{ $social_youtube }}" target="_blank"><i class='fa fa-youtube'></i></a> @endif
			  </div>
		   </div>  
		   		   		   
		</div>
	 </div> 
 </div>
 
 <div class='footerContainer'>  
  <div id="footer-txt"> 
	@if ( $company_name != "")<a href="{{ url('') }}">&copy; {{ date('Y') }} {{ $company_name }} Pty Ltd</a> | @endif 
    <a href="http://2cbms.2construct.com.au" target='_blank'>Staff Login</a> | 
    <!--<a href="{{ url('') }}/contact">Contact</a> | -->    
    <a href="https://www.echo3.com.au" target="_blank">Website by Echo3</a> 
    
  </div>
  </div>
</footer>