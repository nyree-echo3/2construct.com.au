<div class="panelNav">
	<div id="introWrapper">
		<div class='homePages'>	               
			  <div class="row">         
				 <div class="col-lg-3">
					<div class="homePages-header">
					   <h2>Our</h2>		
					   <h2><span class="color-alt">Expertise</span></h2>		
					</div>
				 </div>	

				 @php
					 $pageCounter = 0;
				 @endphp

				 @foreach($home_pages as $home_page)
					 <div class="col-lg-3 col-border">
						 <h3><span class="{{ ($pageCounter == 0 ? 'color-alt' : '') }}">{{ $home_page->title }}</span></h3>
						 <div>{!! $home_page->short_description !!}</div>

						 <div class='homePage-mobile-more'>
							<a href='{{ url('') }}/pages/{{ $home_page->category->slug }}/{{ $home_page->slug }}'>Learn More ></a>				
						 </div>
					 </div>	
					 @php
					 $pageCounter++;
					 if ($pageCounter == 3) {
						break;
					 } 
					 @endphp
				 @endforeach          

				 <div class="col-lg-3">
					<div class="homePages-header">			   	
					</div>
				 </div>	

				 @foreach($home_pages as $home_page)                   
					 <div class="col-lg-3 col-border homePage-web-more">							    
						<a href='{{ url('') }}/pages/{{ $home_page->category->slug }}/{{ $home_page->slug }}'>Learn More ></a>
					 </div>			     
					 @php
					 $pageCounter++;
					 if ($pageCounter == 3) {
						break;
					 } 
					 @endphp
				 @endforeach                 	    
			  </div>
		  </div>
	</div>  
</div>  