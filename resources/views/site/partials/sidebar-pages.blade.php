<div class="col-sm-3 blog-sidebar">
    <div class="sidebar-module">
        <h4>{{ $category[0]->name }}</h4>
        <ol class="navsidebar list-unstyled">
            @foreach ($side_nav as $item)

                @if (strtolower($category[0]->name) == "about" && $item->slug == "community")
					<li class='{{ ($page->slug == "awards" ? "active" : "") }}'>
						<a class="navsidebar" href="{{ url('') }}/projects/awards">Awards</a>
					</li>
					<!--<li class='{{ ($page->slug == "testimonials" ? "active" : "") }}'>
						<a class="navsidebar" href="{{ url('') }}/testimonials">Testimonials</a>
					</li>-->
				@endif
                    
                @if($item->slug=='our-team')
                    <li class='{{ ($item->slug == $page->slug && $selected_category_slug == "2construct-board" ? "active" : "") }}'>
                        <a class="navsidebar" href="{{ url('') }}/team/2construct-board">2Construct Board</a>
                    </li>
                    <li class='{{ ($item->slug == $page->slug && $selected_category_slug == "2construct-executive-team" ? "active" : "") }}'>
                        <a class="navsidebar" href="{{ url('') }}/team/2construct-executive-team">2Construct Executive Team</a>
                    </li>
                @else
                    <li class='{{ ($item->slug == $page->slug ? "active" : "") }}'>
                        <a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item->slug }}">{{ $item->title }}</a>
                    </li>
                @endif                
                    
                    <ol class="navsidebar navsidebar-sub list-unstyled">
                        @foreach ($item->nav_sub as $item_sub)
                            <li class='{{ ($item_sub->slug == $page->slug ? "active" : "") }}'>
                                <a class="navsidebar" href="{{ url('') }}/pages/{{ $category[0]->slug }}/{{ $item_sub->slug }}">{{ $item_sub->title }}</a>
                            </li>
                        @endforeach
                    </ol>
                    @endforeach
        </ol>
    </div>
</div>