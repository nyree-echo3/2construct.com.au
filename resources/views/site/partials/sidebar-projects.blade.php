<div class="col-sm-3 blog-sidebar">         
  <div class="sidebar-module">
	<h4>Projects</h4>

	<ol class="navsidebar list-unstyled">
	  @foreach ($side_nav as $item)
	     @if ($item->slug != "awards")
		 <li class='{{ (!isset($sector_name) && $category_slug ==  $item->slug ? "active" : "") }}'><a class="navsidebar" href="{{ url('') }}/projects/{{ $item->slug }}">{{ $item->name }}</a>
		 		 		 
			@switch ($item->id)
			   @case (1) @php $project_sectors = $project_sectors_civil @endphp @break
			   @case (2) @php $project_sectors = $project_sectors_building @endphp @break
			   @case (3) @php $project_sectors = $project_sectors_civic @endphp @break									               
			@endswitch										       										       

			@if (isset($project_sectors) && $item->slug  == "building-projects")										       
				<ol class="navsidebar list-unstyled">		
				   @foreach ($project_sectors as $project_sector)										
					  <li class='{{ ($category_slug ==  $item->slug && isset($sector_name) && $sector_name ==  $project_sector["sector"] ? "active" : "") }}'><a class="dropdown-item" href="{{ url('') }}/sectors/{{ $item->slug }}/{{ str_replace(" ", "-", strtolower($project_sector["sector"])) }}">{{ $project_sector["sector"] }}</a></li>
				   @endforeach												   												 
				</ol>						
			@endif						
													 
		 </li>
		 @endif
	  @endforeach 	                                      
	</ol>		
  </div>          
</div>