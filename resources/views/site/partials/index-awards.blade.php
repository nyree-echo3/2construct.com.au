@if(isset($home_projects))
     <div class="panelNav panelNav-no-border">
		 <div class=" container home-projects ">
		   <div class="home-projects-row home-projects-row-no-border">
			  <h2>Awards</h2>
			  <div class="row">  		  
				 @php
					$colCounter = 0;					
				 @endphp

				 @foreach($home_awards as $item) 

					  <div class="col-lg-4">
						   <a href='{{ url('') }}/projects/{{ $item->category->slug }}/{{ $item->slug }}'>				
						       <div class="home-projects-a home-project-a-{{ $colCounter }}">
								 <div class="div-img">
								    @if (sizeof($item->images) > 0)
									    <img src="{{ url('') }}/{{$item->images[0]->location}}" alt="{{ $item->title }}">
									@endif 
								 </div>
								 <div class="home-projects-txt">
						            <div class="home-projects-txt-h1">{{ $item->title }}</div>
								    <div class="home-projects-txt-h2">{{ $item->client }}, {{ $item->location }}</div>
							     </div>   
							   </div>							
						   </a>     				   					 					   					   					  
					  </div>

					  @php 
					  $colCounter++;
					  @endphp
				 @endforeach 	
				</div>
		   </div>
		</div>
    </div>
    <div class="slide-padding"></div>
    <div class="awards-line"></div>
@endif