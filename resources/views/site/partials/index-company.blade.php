<div id="introWrapperDIV" class="panelNav">
	<div class="container marketing">						
		<div class="row featurette intro-container">
			<div id="introWrapper">
				<div id="intro">		   
				   <div class="introPanelTwo">
					  <div class="introPanelTwo-txt">
						 {!! $home_intro_text !!}
					  </div>
				   </div>

				   <div class="introPanelOne"><img alt="" src="{{ url('') }}/images/site/pic1.jpg" title="What we do" alt="What we do" /></div>
				</div>
		   </div>
		</div>
	</div>						
</div>


@section('inline-scripts-index')  
   <script type="text/javascript"> 
	   $(document).ready(function() {
		   if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|Windows Phone/i.test(navigator.userAgent) == false) {
			   if (window.innerWidth > 991)  {
				   $header = 100;
				   $image = 1155;
				   $companySection = 380;

				   $diff = ($header + $image) - window.innerHeight;
				   $stopPoint = $diff + $companySection;
				   $stopPointMargin = $stopPoint + 40;

				   $("#divIndex").delay(1500).animate({top: '-=' + $stopPoint + 'px'}, 1200, function(){
					  $("#divIndex").css("margin-bottom", "-" + $stopPointMargin + 'px'); 
				   });	   			   		 
			  }
		  }
	   });   
</script>			
@endsection