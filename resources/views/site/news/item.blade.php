<?php 
   // Set Meta Tags
   $meta_title_inner = $news_item->meta_title; 
   $meta_keywords_inner = $news_item->meta_keywords; 
   $meta_description_inner = $news_item->meta_description;  
?>
@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">               
        
        <div class="col-sm-8 blog-main blog-main-full">

          <div class="blog-post">  
            <div class="blog-post-news">
                <div class="blog-post-news-header-sub">
                NEWS FROM 2CONSTRUCT      
				</div>
               
                <div class="blog-post-news-header">
                	<img src="{{ url('') }}/images/site/newsletter-logo.png" alt="2C Newsletter">
                	
                	<span class="blog-post-news-header-date">{{date("F Y", strtotime($news_item->start_date))}}</span>
                </div>         
                           
				<h1 class="blog-post-title">{{ $news_item->title }}</h1>
                {!! $news_item->body !!}						 		
			  </div><!-- /.blog-post-title -->	           	
         
              <div class='btn-back'>
				<a class='btn-back' href='{{ url('') }}/news'>< Back</a>	
			 </div>	
         
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@endsection
