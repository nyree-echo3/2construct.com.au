@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')

<div class="blog-masthead ">         
    <div class="container">

      <div class="row">      
        @include('site/partials/sidebar-news')
                     
        <div class="col-sm-8 blog-main">

          <div class="blog-post">   
               <h1 class="blog-post-title">Newsletter Subscription</h1>    
               <p>Thanks so much for joining our list, your subscription was a success.</p>             
            </div>
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->    
@endsection            
