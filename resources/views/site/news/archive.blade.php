<?php 
   // Set Meta Tags
   $meta_title_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News"); 
   $meta_keywords_inner = "News"; 
   $meta_description_inner = ($category_name == "Latest News" ? $category_name : $category_name . " - News");  
?>

@extends('site/layouts/app')

@section('content')

@include('site/partials/carousel-inner')


<div class="blog-masthead ">         
    <div class="container">

      <div class="row">        
        
        <div class="col-sm-12 blog-main">

          <div class="blog-post">     
            <h1 class="blog-post-title">{{ $category_name }}</h1>
			          
			<a class='btn-back' href='{{ url('') }}/news'>View Latest <i class='fa fa-chevron-right'></i></a>				
          
           <div class="blog-post row">           
                               	        
            @if(isset($items))  
                 @foreach($items as $item)                
                    <div class="col-sm-3">                       
                       <div class="panel-news-item">	
                            <div class="div-img">
                            <img src="{{ url('') }}/{{ $item->thumbnail }}" alt="{{$item->title}}" />	
                            </div>				                                    
                            
                            <div class="panel-news-item-title">{{$item->title}}</div>
                            <div class="panel-news-item-shortdesc">{!! $item->short_description !!}</div>
                                                        
                      </div>                                                 		
                    </div>                    
                               
				 @endforeach
           
                   <!-- Pagination -->
                   <div id="pagination">{{ $items->links() }}</div>
              
               @else
                 <p>Currently there is no news items to display.</p>    
               @endif
          
             </div><!-- /.blog-post -->
          </div><!-- /.blog-post -->         
        </div><!-- /.blog-main -->        

      </div><!-- /.row -->

    </div><!-- /.container -->
</div><!-- /.blog-masthead -->
    
@endsection
