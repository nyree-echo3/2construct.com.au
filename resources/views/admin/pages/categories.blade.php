@extends('admin/layouts/app')

@section('content')
    <div class="content-wrapper">
        <section class="content-header">
            <h1>Page Categories</h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fas fa-file-alt"></i> Page Categories</a></li>
            </ol>
        </section>

        <section class="content">
            <div class="box">
                <div class="box-header">
                    <h3 class="box-title">Page Category List</h3>

                    @can('add-page-category')
                    <div class="pull-right box-tools">
                        <a href="{{ url('dreamcms/pages/add-category') }}" type="button" class="btn btn-info btn-sm"
                           data-widget="add">Add New
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                    @endcan
                </div>
                <div class="box-body">
                    @if(count($categories))
                        <table class="table table-hover">
                            <tr>
                                <th>Category</th>
                                <th class="pull-right">Actions</th>
                            </tr>
                            @foreach($categories as $category)
                                <tr>
                                    <td>{{ $category->name }}</td>
                                    <td>
                                        <div class="pull-right">
                                        @can('edit-page-category')
                                            <a href="{{ url('dreamcms/pages/'.$category->id.'/edit-category') }}"
                                               class="tool" data-toggle="tooltip" title="Edit">
                                                <i class="fa fa-edit"></i></a>
                                        @endcan
                                        @can('delete-page-category')
                                            <a href="{{ url('dreamcms/pages/'.$category->id.'/delete-category') }}"
                                               class="tool" data-toggle=confirmation data-title="Are you sure?"
                                               data-popout="true" data-singleton="true" data-btn-ok-label="Yes"
                                               data-btn-cancel-label="No"><i class="far fa-trash-alt"></i></a>
                                        @endcan
                                        </div>
                                    </td>

                                </tr>
                            @endforeach
                        </table>
                    @else
                        No records
                    @endif
                </div>
            </div>
        </section>
    </div>
@endsection
@section('scripts')
    <script src="{{ asset('/components/bootstrap-confirmation2/bootstrap-confirmation.min.js') }}"></script>
@endsection
@section('inline-scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle=confirmation]').confirmation({
                rootSelector: '[data-toggle=confirmation]'
            });
        });
    </script>
@endsection