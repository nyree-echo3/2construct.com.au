<?php 
// Permanent Redirects For Old Website
Route::redirect('contact', '/', 301);
Route::redirect('our-projects', '/projects', 301);
Route::redirect('image-gallery', '/projects', 301);
Route::redirect('latest-news', '/news', 301);
Route::redirect('archived-news', '/news/archive', 301);
Route::redirect('our-projects/building', '/projects/building-projects', 301);
Route::redirect('our-projects/civic', '/projects/civic-construction', 301);
Route::redirect('our-projects/civil', '/projects/civil--infrastructure', 301);
Route::redirect('image-gallery/civic', '/projects/civic-construction', 301);
Route::redirect('our-projects/sport', '/sectors/building-projects/sport-&-recreation', 301);
Route::redirect('image-gallery/education', '/sectors/building-project
s/education', 301);
Route::redirect('image-gallery/building', '/projects/building-projects', 301);
Route::redirect('our-projects/education', '/sectors/building-projects/education
', 301);
Route::redirect('image-gallery/civil', '/projects/civil--infrastructure', 301);
Route::redirect('image-gallery/civil', '/projects/civil--infrastructure', 301);
Route::redirect('archived-news/12', '/news/archive', 301);
Route::redirect('latest-news/september-newsletter', '/news/newsletter/2c-news---september-2018', 301);
Route::redirect('latest-news/july-newsletter', '/news/newsletter/2c-news---july-2018', 301);
Route::redirect('latest-news/august-newsletter', '/news/newsletter/2c-news---august-2018', 301);
Route::redirect('latest-news/august-newsletter', '/news/newsletter/2c-news---august-2018', 301);
Route::redirect('image-gallery/building/2', '/projects/building-projects?page=2', 301);
Route::redirect('image-gallery/building/4', '/projects/building-projects?page=4', 301);
Route::redirect('image-gallery/building/10', '/projects/building-projects?page=10', 301);
Route::redirect('image-gallery/education/4', '/sectors/building-projects/education?page=4', 301);
Route::redirect('image-gallery/building/7', '/projects/building-projects?page=7', 301);
Route::redirect('image-gallery/building/9', '/projects/building-projects?page=9', 301);
Route::redirect('image-gallery/building/8', '/projects/building-projects?page=8', 301);
Route::redirect('image-gallery/education/5', '/sectors/building-projects/education?page=5', 301);
Route::redirect('image-gallery/civic/2', '/projects/civic-construction?page=2', 301);
//Route::redirect('image-gallery/civic/3', '/projects/civic-construction?page=3', 301);
Route::redirect('pages/our-services/civic-construction', '/pages/expertise/civic-construction', 301);
Route::redirect('pages/our-services/building-projects', '/pages/expertise/building-projects', 301);
Route::redirect('pages/about-us/credentials-accreditations', '/pages/about/affiliations', 301);
Route::redirect('pages/about-us/about-the-company', '/pages/about/our-history', 301);
Route::redirect('pages/about-us/2construct-pty-ltd-board', '/team/2construct-board', 301);

// Home Page
Route::get('/', 'HomeController@index');

// Newsletter Email
Route::get('/newsletter-email', 'HomeController@newsletter');

// Content Styles
Route::get('/style-guide', 'HomeController@styleGuide');

// Invitation
Route::get('/invite-yes', 'HomeController@inviteYes');
Route::get('/invite-no', 'HomeController@inviteNo');

// Contact Form
Route::group(['prefix' => '/contact'], function () {
    Route::get('/', 'ContactController@index');
    Route::post('save-message', 'ContactController@saveMessage');
    Route::get('success', 'ContactController@success');
});

// Pages Module
Route::group(['prefix' => '/pages'], function () {
    Route::get('{category}', 'PagesController@index');    
	Route::get('{category}/{page}', 'PagesController@index');    
});


// News Module
Route::group(['prefix' => '/news'], function () {
	Route::get('', 'NewsController@list'); 	
	
	Route::get('archive', 'NewsController@archive'); 
	Route::get('archive/{age}', 'NewsController@archive'); 
	
    Route::get('{category}', 'NewsController@list');    
	Route::get('{category}/{page}', 'NewsController@item'); 
	
	Route::get('success', 'NewsController@success');	
});

// Newsletter Module
Route::group(['prefix' => '/newsletter'], function () {	
	Route::get('success', 'NewsController@success');	
});

// Faqs Module
Route::group(['prefix' => '/faqs'], function () {
	Route::get('', 'FaqsController@index'); 
    Route::get('{category}', 'FaqsController@index');    	
});

// Documents Module
Route::group(['prefix' => '/documents'], function () {
	Route::get('', 'DocumentsController@index'); 
    Route::get('{category}', 'DocumentsController@index');    	
});

// Gallery Module
Route::group(['prefix' => '/gallery'], function () {
	Route::get('', 'GalleryController@index'); 
    Route::get('{category}', 'GalleryController@detail');    	
});

// Projects Module
Route::group(['prefix' => '/projects'], function () {
	Route::get('', 'ProjectsController@list'); 			
	
    Route::get('{category}', 'ProjectsController@list');    
	Route::get('{category}/{page}', 'ProjectsController@item'); 
	
});

// Sectors (Projects Module)
Route::group(['prefix' => '/sectors'], function () {
	Route::get('', 'ProjectsController@sectorslist'); 			
	
    Route::get('{category}/{sector}', 'ProjectsController@sectorslist');    
	Route::get('{category}/{sector}/{page}', 'ProjectsController@sectorsitem'); 
	
});

// Links Module
Route::group(['prefix' => '/links'], function () {
	Route::get('', 'LinksController@list'); 				
    Route::get('{category}', 'LinksController@list');    		
});

// Testimonials Module
Route::group(['prefix' => '/testimonials'], function () {
	Route::get('', 'TestimonialsController@index');     
});

// Team Module
Route::group(['prefix' => '/team'], function () {
    Route::get('', 'TeamController@index');
    Route::get('{category}', 'TeamController@index');
    Route::get('{category}/{member}', 'TeamController@member');
});