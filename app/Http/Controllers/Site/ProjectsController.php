<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Project;
use App\ProjectCategory;
use App\Module;
use App\Page;

class ProjectsController extends Controller
{
    public function list(Request $request, $category_slug = "", $item_slug = ""){   
		$module = Module::where('slug', '=', "projects")->first();
		
		if ($category_slug != "awards") {
			// Projects
			$side_nav = $this->getCategories();	

			if (sizeof($side_nav) > 0)  {
				if ($category_slug == "")  {
				   // Get Latest Projects
				   $category_name = "Project Gallery";	

				   $items = $this->getProjects("", 6);
				} elseif ($category_slug != "" && $item_slug == "") {
				  // Get Category Projects	
				  $category = $this->getCategory($category_slug);
				  $category_name = $category->name;	

				  $items = $this->getProjects($category->id, 6);			  
				} 		
			}
			
			// Project Sections			
			$project_sectors_civic = $this->getProjectSectors(3);
			$project_sectors_building = $this->getProjectSectors(2);
			$project_sectors_civil = $this->getProjectSectors(1);
			
			return view('site/projects/list', array( 
				'module' => $module,
				'side_nav' => $side_nav,
				'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
				'items' => (sizeof($side_nav) > 0 ? $items : null),			
				'project_sectors_civic' => $project_sectors_civic,
				'project_sectors_building' => $project_sectors_building,
				'project_sectors_civil' => $project_sectors_civil,
				'category_slug' => $category_slug
			));
			
		} else  {
			// Pages
			$pages = new PagesController();			    	

			$category = $pages->getCategory("about");

			$side_nav = $pages->getPages($category[0]->id);
			$page = $pages->getPages($category[0]->id)->first();
			$page->slug = "awards";
			
			// Projects - Awards			
		    $items = $this->getProjects(4, 6);	
			
			return view('site/projects/list', array(
				'page_type' => "Pages",
				'side_nav' => $side_nav,
				'category' => $category,
				'category_name' => "Project Awards",
				'page' => $page,
				'mode' => "",
				'items' => $items,	
			));		
		}				
    }
	
    public function item(Request $request, $category_slug, $item_slug){
		$module = Module::where('slug', '=', "projects")->first();
		
		if ($category_slug != "awards") {
			// Projects
			$side_nav = $this->getCategories();				  			
			$project_item = $this->getProjectItem($item_slug);	
			
			// Project Sections			
			$project_sectors_civic = $this->getProjectSectors(3);
			$project_sectors_building = $this->getProjectSectors(2);
			$project_sectors_civil = $this->getProjectSectors(1);	

			return view('site/projects/item', array(  
				'module' => $module,
				'side_nav' => $side_nav,					
				'project_item' => $project_item,	
				'category_name' => "",
				'project_sectors_civic' => $project_sectors_civic,
				'project_sectors_building' => $project_sectors_building,
				'project_sectors_civil' => $project_sectors_civil,
				'category_slug' => $category_slug,
				//'sector_name' => ucwords(str_replace("-", " ", str_replace("-and-", "-&-", $sector_slug))),
				//'sector_slug' => $sector_slug
			));
		} else  {
			// Pages
			$pages = new PagesController();			    	

			$category = $pages->getCategory("about");

			$side_nav = $pages->getPages($category[0]->id);
			$page = $pages->getPages($category[0]->id)->first();
			$page->slug = "awards";
			
			// Projects - Awards			
		    $project_item = $this->getProjectItem($item_slug);	
			
			return view('site/projects/item', array(
				'page_type' => "Pages",
				'side_nav' => $side_nav,
				'category' => $category,
				'category_name' => "Awards",
				'page' => $page,
				'mode' => "",
				'project_item' => $project_item,			
			));		
			
		}
		
    }		
	
	public function getCategories(){
		$categories = ProjectCategory::where('status', '=', 'active')->orderBy('position', 'desc')->get();		
		return($categories);
	}	
	
	public function getProjects($category_id = "", $limit = 16){
		if ($category_id == "")  {
			$projects = Project::where('status', '=', 'active')	
				        ->where('category_id', '<>', 4)
				        ->with("images")
						//->orderBy('position', 'desc')
				        ->orderBy('completion_date', 'desc')
						->get();
                        //->paginate($limit);
		} else {
		   $projects = Project::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
			            ->with("images")
						//->orderBy('position', 'desc')
			            ->orderBy('completion_date', 'desc')
                        ->get();
						//->paginate($limit);
		}
		
		return($projects);
	}

    public function getHomeProjects($category_id = "", $limit = 16){
        if ($category_id == "")  {
            $projects = Project::where('status', '=', 'active')
                ->where('category_id', '<>', 4)
                ->with("images")
                //->orderBy('position', 'desc')
                ->orderBy('completion_date', 'desc')
                ->paginate($limit);
        } else {
            $projects = Project::where('status', '=', 'active')
                ->where('category_id', '=', $category_id)
                ->with("images")
                //->orderBy('position', 'desc')
                ->orderBy('completion_date', 'desc')
                ->paginate($limit);
        }

        return($projects);
    }
	
	public function getProjectsBySector($category = "", $sector = "", $limit = 16){				
		if ($sector == "")  {
			$projects = Project::where('status', '=', 'active')	
				        ->where('category_id', '<>', 4)
				        ->with("images")
						//->orderBy('position', 'desc')
				        ->orderBy('completion_date', 'desc')
                        ->get();
						//->paginate($limit);
		} else {

		   $projects = Project::where('status', '=', 'active')
			            ->whereJsonContains('sector_json', $sector)
			            ->where('category_id', '=', $category)
			            ->with("images")
						//->orderBy('position', 'desc')
			            ->orderBy('completion_date', 'desc')
                        ->get();
						//->paginate($limit);
		}
		
		return($projects);
	}	
	
	public function getFeaturedProjects($category_id = "", $limit = 2){
		if ($category_id == "")  {
			$projects = Project::where(['status' => 'active', 'feature' => 'active'] )	
				        ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);	
		} else {
		   $projects = Project::where(['status' => 'active', 'feature' => 'active'])
			            ->where('category_id', '=', $category_id)						
			            ->with("images")
						->orderBy('position', 'desc')
						->paginate($limit);		
		}
		
		return($projects);
	}
	  
	public function getProjectItem($item_slug){		
		$project = Project::where(['status' => 'active', 'slug' => $item_slug])
			        ->where('status', '=', 'active')
			        ->first();						
		return($project);
	}
	
	public function getCategory($category_slug){
		$categories = ProjectCategory::where('slug', '=', $category_slug)->first();		
		return($categories);
	}	
	
	// SECTORS
	public function sectorslist(Request $request, $category_slug = "", $sector_slug = "", $item_slug = ""){   
		$module = Module::where('slug', '=', "projects")->first();
		
		$category = $this->getCategory($category_slug);
		$category_id = $category->id;	
		
		// Projects
		$side_nav = $this->getCategories();

		if (sizeof($side_nav) > 0)  {
			if ($sector_slug == "")  {
			   // Get Latest Projects
			   $category_name = "Project Gallery";	

			   $items = $this->getProjects("", 6);
			} elseif ($sector_slug != "" && $item_slug == "") {
			  // Get Category Projects	
			  $sector = ucwords(str_replace("-", " ", str_replace("-and-", "-&-", $sector_slug)));							  			  

			  $items = $this->getProjectsBySector($category_id, $sector, 6);			  
				
			  if (sizeof($items) > 0)  {	
			     $category_name = $items[0]->category->name;
			  } else  {
				 $category_name = ""; 
			  }
			} 		
		}

		// Project Sections			
		$project_sectors_civic = $this->getProjectSectors(3);
		$project_sectors_building = $this->getProjectSectors(2);
		$project_sectors_civil = $this->getProjectSectors(1);

		return view('site/projects/sectorslist', array( 
			'module' => $module,
			'side_nav' => $side_nav,
			'category_name' => (sizeof($side_nav) > 0 ? $category_name : null),
			'items' => (sizeof($side_nav) > 0 ? $items : null),	
			'project_sectors_civic' => $project_sectors_civic,
			'project_sectors_building' => $project_sectors_building,
			'project_sectors_civil' => $project_sectors_civil,
			'sector_name' => $sector,
			'category_slug' => $category_slug,
			'sector_slug' => $sector_slug
		));
	}
	
	public function sectorsitem(Request $request, $category_slug = "", $sector_slug, $item_slug){
		$module = Module::where('slug', '=', "projects")->first();
				
		$side_nav = $this->getCategories();				  			
		$project_item = $this->getProjectItem($item_slug);		
		
		// Project Sections			
		$project_sectors_civic = $this->getProjectSectors(3);
		$project_sectors_building = $this->getProjectSectors(2);
		$project_sectors_civil = $this->getProjectSectors(1);		

		return view('site/projects/sectorsitem', array(  
			'module' => $module,
			'side_nav' => $side_nav,					
			'project_item' => $project_item,	
			'category_name' => "",
			'project_sectors_civic' => $project_sectors_civic,
			'project_sectors_building' => $project_sectors_building,
			'project_sectors_civil' => $project_sectors_civil,
			'category_slug' => $category_slug,
			'sector_name' => ucwords(str_replace("-", " ", str_replace("-and-", "-&-", $sector_slug))),
			'sector_slug' => $sector_slug
		));		
		
    }		
	
	public function getProjectSectors($category_id)  {
		$sectors = Project::select('sector')->where('category_id', '=', $category_id)->distinct('sector')->orderBy('sector')->whereNotNull('sector')->get()->toArray();
		return($sectors);
	}
}
