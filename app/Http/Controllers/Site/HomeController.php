<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index(){

    	return view('site/index');

    }
	
	public function styleGuide(){

    	return view('site/style-guide');

    }
	
	public function newsletter(){

    	return view('site/newsletter-email');

    }
	
	public function inviteYes(){

    	return view('site/invite-response', array(
            'response' => "yes",			
        ));

    }
	
	public function inviteNo(){

    	return view('site/invite-response', array(
            'response' => "no",			
        ));


    }
}
