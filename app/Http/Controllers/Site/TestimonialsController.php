<?php

namespace App\Http\Controllers\Site;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Testimonial;
use App\Module;
use App\Page;

class TestimonialsController extends Controller
{
    public function index($mode = ""){	
		$module = Module::where('slug', '=', "testimonials")->first();
		
		// Pages
		$pages = new PagesController();			    	
		
		$category = $pages->getCategory("about");
		
		$side_nav = $pages->getPages($category[0]->id);
		$page = $pages->getPages($category[0]->id)->first();
		$page->slug = "testimonials";
		
		// Testimonials
		$items = $this->getItems($mode);	
		
		return view('site/testimonials/list', array(
            'page_type' => "Pages",
			'side_nav' => $side_nav,
			'category' => $category,			
			'page' => $page,
			'mode' => $mode,
			'items' => $items,	
        ));				

    }
		
	public function getItems($mode){
		if ($mode == "preview") {
		   $items = Testimonial::orderBy('position', 'asc')->get();						
		} else {
		   $items = Testimonial::where('status', '=', 'active')->orderBy('position', 'asc')->get();						
		}
		return($items);
	}		
}
