<?php

namespace App\Http\Controllers\Site;

use App\Helpers\NavigationBuilder;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Link;
use App\LinkCategory;
use App\Module;

class LinksController extends Controller
{   
	
	 public function list(Request $request, $category_slug = ""){   
		$module = Module::where('slug', '=', "links")->first();
				
        $client_links = $this->getLinks(1);
		$partner_links = $this->getLinks(2);
		 
		return view('site/links/list', array( 
			'module' => $module,			
			'client_links' => $client_links ,						
			'partner_links' => $partner_links ,		
		));					
    }
	
	public function getLinks($category_id = "", $limit = 50){
		if ($category_id == "")  {
			$links = Link::where('status', '=', 'active')						
						->orderBy('position', 'asc')
				        ->paginate($limit);	
		} else {
		   $links = Link::where('status', '=', 'active')
			            ->where('category_id', '=', $category_id)						
						->orderBy('position', 'asc')
			            ->paginate($limit);	
		}		
		return($links);
	}		  	
}
