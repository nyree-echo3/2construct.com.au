<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Module;
use Illuminate\Http\Request;
use Validator, Illuminate\Support\Facades\Input, Redirect;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Response;

class ModulesController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request)
    {

        $modules = Module::orderBy('position', 'desc')->get();
        return view('admin/modules/modules', array(
            'modules' => $modules
        ));
    }

    public function changeStatus(Request $request, $module_id)
    {
        $module = Module::where('id', '=', $module_id)->first();
        if ($request->status == "true") {
            $module->status = 'active';
        } else if ($request->status == "false") {
            $module->status = 'passive';
        }
        $module->save();

        return Response::json(['status' => 'success']);
    }

}