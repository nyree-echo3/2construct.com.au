<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Rutorika\Sortable\SortableTrait;

class DocumentCategory extends Model
{
    use SortableTrait;

    protected $table = 'document_categories';

    public function newQuery()
    {
        return parent::newQuery()->where('is_deleted','=','false');
    }

    public function documents()
    {
        return $this->hasMany(Document::class, 'category_id');
    }
}
