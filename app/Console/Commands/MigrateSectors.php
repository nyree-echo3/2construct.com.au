<?php

namespace App\Console\Commands;

use App\Project;
use Illuminate\Console\Command;

class MigrateSectors extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'migrate:sectors';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $projects = Project::get();

        foreach ($projects as $project){

            $project->sector_json = '["'.$project->sector.'"]';
            $project->save();

        }

    }
}
